package com.example.IhateFrogs.controller;

import com.example.IhateFrogs.DataBase.DataBase;
import com.example.IhateFrogs.Violation.Violation;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/v1/fines")
public class UserController {

    // я немного не понял, как должен обновляться и удаляться штраф, так как в задании не было описано id штрафа
    // поэтому тут я создал контейнер, где храниться штраф, который был вызван в методе get по id
    private static Violation fineContainer = null;

    @GetMapping()
    public String getFine() {
        return DataBase.getData();
    }

    @PostMapping()
    public String printFine(@RequestBody Violation fines) {
        DataBase.postNewViolation(fines);
        return "Штраф с id = " + fines.getId() + " добавлен!";

    }

    @GetMapping("/{fineId}")
    public String printById(@PathVariable("fineId") int id) {

        if (DataBase.getbyID(id) != null) {
            fineContainer = DataBase.getbyID(id);
            return fineContainer.toString();
        }
        fineContainer = null;
        return "¯\\_(ツ)_/¯ такого штрафа нет в базе.";
    }

    @DeleteMapping()
    public String deleteContainer() {
        if (fineContainer == null) {
            return "Сначала получите конкретный штраф через GET запрос по ID";
        }
        DataBase.deleteFine(fineContainer);
        fineContainer = null;
        return "Штраф удалён.";
    }

    @PutMapping()
    public String putContainer(@RequestBody Violation object) {
        if (fineContainer == null) {
            return "Сначала получите конкретный штраф через GET запрос по ID, а затем введите его новую конфигурацию.";
        }
        fineContainer.updateObject(object);
        DataBase.updateFine(fineContainer);
        fineContainer = null;
        return "Объект обновлен";
    }

    @PatchMapping("/{fineId}/pay")
    public String payFine(@PathVariable("fineId") int id) {
        if (DataBase.getbyID(id) != null && !DataBase.getbyID(id).getPayment()) {
            DataBase.getbyID(id).setPayment(true);
            return "Штраф оплачен";
        }
        return "Таокого id нет в базе или штраф уже оплачен.";
    }

    @PatchMapping("/{fineId}/court")
    public String sendSubpoena(@PathVariable("fineId") int id) {
        if (DataBase.getbyID(id) != null && !DataBase.getbyID(id).getSubpoena()) {
            DataBase.getbyID(id).setSubpoena(true);
            return "Повестка отправлена";
        }
        return "Такого id нет в базе или повестка уже отправлена.";
    }
}







