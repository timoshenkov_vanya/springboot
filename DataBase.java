package com.example.IhateFrogs.DataBase;

import com.example.IhateFrogs.Violation.Violation;

import java.util.ArrayList;


public class DataBase {


    private static ArrayList<Violation> Data = new ArrayList<>();


    public static String getData() {
        StringBuilder stringOfData = new StringBuilder();
        if (Data.size() != 0) {
            for (Violation datum : Data) {
                stringOfData.append(datum.toString()).append("\n\n");

            }
            stringOfData.append("Получены данные о всех нарушителях.");
            return stringOfData.toString();
        }
        return "В базе нет нарушителей.";

    }

    public static void postNewViolation(Violation violation) {
        Data.add(violation);
    }

    public static Violation getbyID(int id) {


        for (Violation fine : Data) {
            if (fine.getId() == id) {
                return fine;
            }
        }
        return null;
    }

    public static void deleteFine(Violation fine) {
        Data.remove(fine);
    }

    public static void updateFine(Violation fine) {
        for (Violation o : Data) {
            if (o.getId() == fine.getId()) {
                o = fine;
            }
        }
    }
}
